(in-package :define-window-manager)

;; Copied-and-modified from McCLIM's CLX backend's port.lisp so that we can intercept the
;; events. EVENT-HANDLER should probably be a generic, with the port as the first argument.
(defmethod get-next-event ((port define-window-manager-port) &key wait-function (timeout nil))
  (declare (ignore wait-function))
  (let* ((clim-clx::*clx-port* port)
         (display (clim-clx::clx-port-display port)))
    (declare (special clim-clx::*clx-port*))
    (unless (xlib:event-listen display)
      (xlib:display-finish-output display))
    ; temporary solution
    (or (xlib:process-event display :timeout timeout :discard-p t
                            :handler (lambda (&rest event-slots)
                                       (apply #'event-handler port event-slots)))
	:timeout)))
;; [Mike] Timeout and wait-functions are both implementation 
;;        specific and hence best done in the backends.


(defclass window-manager-event (event)
  ()
  (:documentation
   "Class to inidicate events that are specifically of interest to
window managers and not to CLIM."))

(defclass window-configuration-mixin ()
  ((x :initarg :x)
   (y :initarg :y)
   (width :initarg :width)
   (height :initarg :height)
   (border-width :initarg :border-width)))

(defclass create-window-event (device-event window-configuration-mixin window-manager-event)
  ((event-sheet :initarg :event-sheet
                :reader event-sheet)
   (new-window :initarg :new-window
               :reader event-new-window)))

(defclass destroy-window-event (device-event window-manager-event)
  ((event-sheet :initarg :event-sheet
                :reader event-sheet)
   (sheet :initarg :sheet)))

(defclass reparent-event (device-event window-manager-event)
  ((event-sheet :initarg :event-sheet)
   (parent :initarg :parent
           :documentation "The new parent")
   (override-redirect-p :initarg :override-redirect-p)))

(defclass unmap-event (device-event window-manager-event)
  ((parent :initarg :parent)
   (configure-p :initarg :configure-p)))

(defclass client-request (device-event window-manager-event)
  ((parent :initarg :parent)))

(defclass circulate-request (client-request window-manager-event)
  ((place :initarg :place)))

(defclass configure-request (client-request window-configuration-mixin window-manager-event)
  ((stack-mode :initarg :stack-mode)
   (above-sibling :initarg :above-sibling)
   (value-mask :initarg :value-mask)))

(defclass map-request (client-request window-manager-event)
  ())

(defclass resize-request (client-request window-configuration-mixin window-manager-event)
  ((width :initarg :width)
   (height :initarg :height)))

(defclass property-change-event (device-event window-manager-event)
  ((atom :initarg :atom
         :reader property-change-event-atom)
   (state :initarg :state
          :reader property-change-event-state)))

(defun event-handler (&rest event-slots
                            &key display event-window window parent
                            event-key code state mode time
                            width height x y root-x root-y border-width
                            data override-redirect-p send-event-p configure-p
                            above-sibling atom place
                            hint-p stack-mode value-mask
                            &allow-other-keys)
  (or (apply 'clim-clx::event-handler event-slots)
      (let ((event-sheet (and event-window
                              (climi::port-lookup-sheet clim-clx::*clx-port* event-window)))
            (sheet (and window
                        (climi::port-lookup-sheet clim-clx::*clx-port* window)))
            (parent-sheet (and parent
                               (climi::port-lookup-sheet clim-clx::*clx-port* parent))))
        (declare (special clim-clx::*clx-port*))
        (when event-sheet
          (case event-key
            (:create-notify
             (make-instance 'create-window-event
                            :sheet event-sheet
                            :new-window window
                            :x x :y y
                            :width width :height height
                            :border-width border-width
                            :override-redirect-p override-redirect-p))
            (:destroy-notify
             (make-instance 'destroy-window-event
                            :parent event-sheet
                            :sheet sheet))
            (:map-request
             (make-instance 'map-request
                            :sheet event-sheet
                            :source-sheet sheet))
            (:reparent-notify
             (make-instance 'reparent-event
                            :sheet event-sheet
                            :source-sheet sheet
                            :parent parent-sheet
                            :override-redirect-p override-redirect-p))
            (:unmap-notify
             (make-instance 'unmap-event
                            :sheet event-sheet
                            :source-sheet sheet
                            :configure-p configure-p))
            (:circulate-request
             (make-instance 'circulate-request
                            :sheet event-sheet
                            :source-sheet sheet
                            :place place))
            (:configure-request
             (make-instance 'configure-request
                            :sheet event-sheet
                            :source-sheet sheet
                            :x x :y y
                            :width width :height height
                            :border-width border-width
                            :stack-mode stack-mode
                            :above-sibling above-sibling
                            :value-mask value-mask))
            (:resize-request
             (make-instance 'resize-request
                            :sheet (climi::port-lookup-sheet clim-clx::*clx-port* (xlib:drawable-root window))
                            :source-sheet sheet
                            :width width :height height))
            (:property-notify
             (make-instance 'property-change-event
                            :sheet sheet
                            :atom atom
                            :state state
                            :timestamp time))
            (t
             nil))))))

(defmethod distribute-event ((port define-window-manager-port) (event window-manager-event))
  (dispatch-event (event-sheet event) event))

;;; What kind of dispatching policy (queueing vs. immediate handling) do we want for the different WM events?


(defmethod handle-event (sheet (event window-manager-event))
  (handle-window-manager-event sheet event))
(defmethod handle-event ((sheet external-sheet) event)
  (handle-window-manager-event (window-border sheet) event))
(defmethod handle-event ((sheet root-sheet) event)
  (handle-window-manager-event sheet event))

(defgeneric handle-window-manager-event (sheet event)
  (:method-combination hooked-method-combination)
  (:documentation
   "This function is called by HANDLE-EVENT for all
window-management-related events. The primary methods of this function
call the appropriate 'action performer' function to perform the actual
event handling for the event. They are naive in that they simply call
that function typically based on the event type, but sometimes based on
some slots of that event. Any standard filtering or massaging of events
is done in the :around or :before methods of this function. Hooks may be
added to further filter or massage the events.

In summary, once the primary method is called, it is assumed that the
event should now be handled as-is. Any hooks on the 'action performer'
functions should be merely for updating data structures, tweaking
properties, or the like, not for substantially affecting the progress or
behavior of the action."))

(defgeneric manage-window (window-manager new-window)
  (:method-combination hooked-method-combination))

(defgeneric unmanage-window (window-manager sheet)
  (:method-combination hooked-method-combination))

(defgeneric configure-window (window-border request)
  (:method-combination hooked-method-combination))

(defmethod handle-window-manager-event ((root root-sheet) (event create-window-event))
  (let ((new-sheet (manage-window (window-manager root) (event-new-window event))))
    (configure-window new-sheet event)))
(defmethod handle-window-manager-event ((sheet window-border) (event reparent-event))
  (unmanage-window (window-manager sheet) (event-source-sheet event)))
(defmethod handle-window-manager-event ((sheet root-sheet) (event destroy-window-event))
  (unmanage-window (window-manager sheet) (event-source-sheet event)))
(defmethod handle-window-manager-event ((sheet window-border) (event circulate-request))
  (configure-window sheet event))
(defmethod handle-window-manager-event ((sheet window-border) (event configure-request))
  (configure-window sheet event))
(defmethod handle-window-manager-event ((sheet window-border) (event resize-request))
  (configure-window sheet event))
(defmethod handle-window-manager-event ((sheet window-border) (event property-change-event))
  (handle-property-change sheet (property-change-event-atom event) event))

(defmethod manage-window ((window-manager define-window-manager) new-window)
  (let ((sheet (make-external-sheet window-manager new-window)))
    (climi::port-register-mirror (port window-manager) sheet new-window)))

(defmethod unmanage-window ((window-manager define-window-manager) sheet)
  (let ((port (port window-manager)))
    (climi::port-unregister-mirror port sheet (climi::port-lookup-sheet port sheet))))

