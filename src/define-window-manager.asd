(asdf:defsystem :define-window-manager
    :components
  ((:file "package")
   (:file "application"
          :depends-on ("package"))
   (:file "events"
          :depends-on ("application"))
   (:file "borders"
          :depends-on ("package")))
  :depends-on (:clim-clx))