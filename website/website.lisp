(asdf:operate 'asdf:load-op :defdoc.contrib.project-website)

(defpackage :define-window-manager.website
    (:use :defdoc :defdoc.elements :defdoc.layout :common-lisp :defdoc.frontends.basic
          :defdoc.contrib.project-website))

(in-package :define-window-manager.website)

(defabbreviation DefDoc (doc italic () "Def") (doc small-caps () "Doc"))
(defabbreviation Define-Window-Manager (doc italic () "Define") "-" (doc small-caps () "Window-Manager"))

(defun gen-src-link (target)
  (concatenate 'string "/cgi-bin/viewcvs.cgi/Define-Window-Manager/src" target "?cvsroot=defwm"
               (unless (eql (aref target (1- (length target))) #\/)
                 "&rev=HEAD&content-type=text/vnd.viewcvs-markup")))

(defun gen-mailing-list-link (target)
  (concatenate 'string "/mailman/listinfo/defwm-" target))

(defdoc index (project-website name (doc Define-Window-Manager)
                               short-description "An extensible, object-oriented, command-oriented X11 window manager"
                               author "Rahul Jain")
  (documentation ()
                 (paragraph ()
                            "None right now."))
  (code ()
        (paragraph ()
                   "The code is in a rather infantile state, but what there is is "
                   (link (url (gen-src-link "/")) "publically available")". "
                   (Define-Window-Manager) " hooks into McCLIM's CLX backend and "
                   "intercepts window-management-related events.")
        (section (title "Plans")
                 (paragraph ())))
  (communication ()
                 (paragraph ()
                            "There are 3 mailing lists for "(Define-Window-Manager)", "
                            (link (url (gen-mailing-list-link "announce")) "defwm-announce")", "
                            (link (url (gen-mailing-list-link "devel")) "defwm-devel")", and "
                            (link (url (gen-mailing-list-link "cvs")) "defwm-cvs")".")))
