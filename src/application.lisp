(in-package :define-window-manager)

(defclass define-window-manager-port (clim-clx::clx-port)
  ()
  (:documentation "Here to allow us to intercept clx events"))

(define-application-frame define-window-manager ()
  ()
  (:pointer-documentation t)
  (:panes
   (interactor :interactor)
   (pointer-documentation :pointer-documentation))
  (:layouts
   (complete
    (vertically ()
              (:fill interactor)
              pointer-documentation))
   (interactor-only
    interactor)
   (documentation-only
    pointer-documentation)
   (hidden
    ())))

(defmethod run-frame-toplevel ((frame define-window-manager))
  (let* ((port (port frame))
         (original-port-class (class-of port)))
    (if (subtypep original-port-class 'clim-clx::clx-port)
        (unwind-protect
            (progn
              (initialize-window-manager frame)
              (call-next-method))
          (uninitialize-window-manager frame original-port-class))
      (error "DefWindowManager must be used with McCLIM-CLX!"))))

(defmethod port-class ((frame define-window-manager))
  'define-window-manager-port)

(defmethod root-sheet-class ((frame define-window-manager) (window t))
  'root-sheet)

(defmethod initialize-window-manager ((frame define-window-manager))
  (change-class (port frame) (port-class frame))
  (multiple-value-bind (children parent root-window)
      (xlib:query-tree (clim-clx::clx-port-window (port frame)))
    (declare (ignore parent))
    (make-instance (root-sheet-class frame root-window) :frame frame :root-window root-window)
    (setf (xlib:window-event-mask root-window)
          (logior (xlib:window-event-mask root-window)
                  (xlib:make-event-mask :substructure-redirect :resize-redirect)))
    (dolist (window children)
      (manage-window window frame))))

(defmethod uninitialize-window-manager ((frame define-window-manager) original-port-class)
  (change-class (port frame) original-port-class)
  (multiple-value-bind (children parent root-window)
      (xlib:query-tree (clim-clx::clx-port-window (port frame)))
    (declare (ignore parent))
    (setf (xlib:window-event-mask root-window)
          (logandc2 (xlib:window-event-mask root-window)
                    (xlib:make-event-mask :substructure-redirect :resize-redirect)))
    (dolist (window children)
      (unmanage-window window frame))))

(defclass external-sheet (mute-repainting-mixin sheet-parent-mixin sheet-leaf-mixin mirrored-sheet-mixin sheet-translation-mixin basic-sheet)
  ())

(defclass root-sheet (sheet-multiple-child-mixin mirrored-sheet-mixin basic-sheet)
  ((frame :initarg :frame)
   (root-window :initarg :root-window)))

;; The standard entry point
(defun start ()
  (run-frame-toplevel
   (make-application-frame 'define-window-manager)))
