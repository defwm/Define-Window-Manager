(defpackage :define-window-manager
  (:use :cl :clim)
  (:shadowing-import-from :clim #:interactive-stream-p))
