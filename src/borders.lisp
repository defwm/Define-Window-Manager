(in-package :define-window-manager)

(defclass window-border (mute-repainting-mixin sheet-parent-mixin sheet-multiple-child-mixin mirrored-sheet-mixin sheet-translation-mixin layout-protocol-mixin basic-sheet)
  ((north-west-pane)
   (north-pane)
   (north-east-pane)
   (west-pane)
   (managed-window :initarg :managed-window)
   (east-pane)
   (south-west-pane)
   (south-pane)
   (south-east-pane)
   (north-space)
   (south-space)
   (east-space)
   (west-space)))

(defmethod compose-space ((border window-border) &key width height)
  (declare (ignore width height))
  (with-slots (north-space south-space west-space east-space
                           north-west-pane north-pane north-east-pane 
                           west-pane managed-window east-pane       
                           south-west-pane south-pane south-east-pane) border
    (let ((nw (compose-space north-west-pane))
          (n  (compose-space north-pane))
          (ne (compose-space north-east-pane))
          (w  (compose-space west-pane))
          (e  (compose-space east-pane))
          (sw (compose-space south-west-pane))
          (s  (compose-space south-pane))
          (se (compose-space south-east-pane)))
      (setf north-space (max (space-requirement-min-height nw)
                             (space-requirement-min-height n)
                             (space-requirement-min-height ne))
            south-space (max (space-requirement-min-height sw)
                             (space-requirement-min-height s)
                             (space-requirement-min-height se))
            west-space  (max (space-requirement-min-width  nw)
                             (space-requirement-min-width  w)
                             (space-requirement-min-width  sw))
            east-space  (max (space-requirement-min-width  ne)
                             (space-requirement-min-width  e)
                             (space-requirement-min-width  se)))
      (let ((border-width (+ west-space east-space))
            (border-height (+ north-space south-space)))
        (space-requirement+* (compose-space managed-window)
                             :width border-width
                             :min-width border-width
                             :max-width border-width
                             :height border-height
                             :min-height border-height
                             :max-height border-height)))))

(defmethod allocate-space ((border window-border) width height)
  (with-slots (north-space south-space west-space east-space
                           north-west-pane north-pane north-east-pane 
                           west-pane managed-window east-pane       
                           south-west-pane south-pane south-east-pane) border
    (let ((inside-top north-space)
          (inside-left west-space)
          (inside-width (- width west-space east-space))
          (inside-height (- height north-space south-space))
          (inside-bottom (- width east-space))
          (inside-right (- height south-space)))
      (layout-child north-west-pane :left   :top    0            0             west-space   north-space  )
      (layout-child north-pane      :center :top    inside-left  0             inside-width north-space  )
      (layout-child north-east-pane :right  :top    inside-right 0             east-space   north-space  )
      (layout-child west-pane       :left   :center 0            inside-top    west-space   inside-height)
      (layout-child managed-window  :center :center inside-left  inside-top    inside-width inside-height)
      (layout-child east-pane       :right  :center inside-right inside-top    east-space   inside-height)
      (layout-child south-west-pane :left   :bottom 0            inside-bottom west-space   south-space  )
      (layout-child south-pane      :center :bottom inside-left  inside-bottom inside-width south-space  )
      (layout-child south-east-pane :right  :bottom inside-right inside-bottom east-space   south-space  ))))

